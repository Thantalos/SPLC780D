
#include "SPLC780D.h"

#define rs LATCbits.LATC4
#define rw LATCbits.LATC5
#define e LATCbits.LATC6
#define d LATD

void
SPLC780D_delay_short() 
{
    uint8_t i;
    for (i = 0; i < 10; i++);
    //__delay_us(29);
}

void
SPLC780D_delay_long() 
{
    uint8_t i;
    for (i = 0; i < 200; i++);
    //__delay_us(1180);
}

void
SPLC780D_execute()
{
    e = 0;
    SPLC780D_delay_short();
    e = 1;
}

void
SPLC780D_execute2()
{
    e = 0;
    SPLC780D_delay_long();
    e = 1;
}

void
SPLC780D_clear_display()
{
    rs = 0;
    rw = 0;
    d = 0b00000001;
    
    SPLC780D_execute2();
}

void
SPLC780D_return_home()
{
    rs = 0;
    rw = 0;
    d = 0b00000010;
    
    SPLC780D_execute2();
}

void
SPLC780D_set_entry_mode(const uint8_t ID, const uint8_t S)
{
    uint8_t tmp = 0b00000100;
    
    set_bit_to(&tmp, 1, ID);
    set_bit_to(&tmp, 0, S);
    
    rs = 0;
    rw = 0;
    d = tmp;
    
    SPLC780D_execute();
}

void
SPLC780D_set_display_control(const uint8_t D, const uint8_t C, const uint8_t B)
{
    uint8_t tmp = 0b00001000;
    
    set_bit_to(&tmp, 2, D);
    set_bit_to(&tmp, 1, C);
    set_bit_to(&tmp, 0, B);
    
    rs = 0;
    rw = 0;
    d = tmp;
    
    SPLC780D_execute();
}

void
SPLC780D_shift_cursor_or_display(const uint8_t SC, const uint8_t RL)
{
    uint8_t tmp = 0b00010000;
    
    set_bit_to(&tmp, 3, SC);
    set_bit_to(&tmp, 2, RL);
    
    rs = 0;
    rw = 0;
    d = tmp;
    
    SPLC780D_execute();
}

void
SPLC780D_set_functions(const uint8_t DL, const uint8_t N, const uint8_t F)
{
    uint8_t tmp = 0b00100000;
    
    set_bit_to(&tmp, 4, DL);
    set_bit_to(&tmp, 3, N);
    set_bit_to(&tmp, 2, F);
    
    rs = 0;
    rw = 0;
    d = tmp;
    
    SPLC780D_execute();
}

void
SPLC780D_set_character_generator_RAM_address(const uint8_t a)
{
    uint8_t tmp = 0b01000000;
    uint8_t a_trimmed;
    
    a_trimmed = a & 0b00111111;
    tmp |= a_trimmed;
    
    rs = 0;
    rw = 0;
    d = tmp;
    
    SPLC780D_execute();
}

void
SPLC780D_set_display_data_RAM_address(const uint8_t a)
{
    uint8_t tmp = 0b10000000;
    uint8_t a_trimmed;
    
    a_trimmed = a & 0b01111111;
    tmp |= a_trimmed;
    
    rs = 0;
    rw = 0;
    d = tmp;
    
    SPLC780D_execute();
}

void
SPLC780D_read_busy_flag_and_address(uint8_t * const BF, uint8_t * const AC)
{
    rs = 0;
    rw = 1;
    
    *BF = PORTDbits.RD7;
    *AC = PORTD & 0b01111111;
    
    SPLC780D_execute();
}

void
SPLC780D_write_data_to_RAM(const uint8_t a)
{
    rs = 1;
    rw = 0;
    d = a;
    
    SPLC780D_execute();
}

void
SPLC780D_read_data_from_RAM(uint8_t * const a)
{
    rs = 1;
    rw = 1;
    *a = d;
    
    SPLC780D_execute();
}

#undef rs
#undef rw
#undef e
#undef d



void
SPLC780D_setIO()
{
    clear_bit(&TRISC, 4);
    clear_bit(&TRISC, 5);
    clear_bit(&TRISC, 6);
    TRISD = 0X00;
}

void
SPLC780D_default_init()
{
    SPLC780D_setIO();
    
    SPLC780D_set_functions(1, 1, 0);
    SPLC780D_set_display_control(1, 0, 0);
    SPLC780D_set_entry_mode(1, 0);
    SPLC780D_clear_display();
    SPLC780D_return_home();
}

void
SPLC780D_display_character(const char c)
{
    // all functions in this library reset the dd position when they are done
    SPLC780D_write_data_to_RAM(c);
}

void
SPLC780D_display_string(char const * const c, const uint8_t l)
{
    int i;
    if (!l)
    {
        SPLC780D_display_string2(c);
        return;
    }
    for (i = 0; i < l; i++)
        SPLC780D_write_data_to_RAM(c[i]);
}

void
SPLC780D_display_string2(char const * c)
{
    while (*c != '\0')
        SPLC780D_write_data_to_RAM(*(c++));
}

void
SPLC780D_display_string3(char const * c)
{
    while (*c != '\255')
        SPLC780D_write_data_to_RAM(*(c++));
}

void
SPLC780D_set_cursor_position(const uint8_t p, const uint8_t l)
{
    SPLC780D_set_display_data_RAM_address(p + 0x40*l);
}

void
SPLC780D_define_custom_char(const char c, const uint8_t l[8])
{
    int i;
    uint8_t BF;
    uint8_t AC;
    
    // remember current position in dd
    SPLC780D_read_busy_flag_and_address(&BF, &AC);
    
    SPLC780D_set_character_generator_RAM_address(c*8);
    
    for (i = 0; i < 8; i++)
        SPLC780D_write_data_to_RAM(l[i]);
    
    // reset dd position
    SPLC780D_set_display_data_RAM_address(AC);
}

void
SPLC780D_read_busy_flag(uint8_t * const BF)
{
    uint8_t AC;
    
    SPLC780D_read_busy_flag_and_address(BF, &AC);
}
