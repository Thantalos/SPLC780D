/* 
 * @file bit_operations.h
 * @author Matthias Hellerer
 * @section license MIT License
 * Copyright (c) 2016 Matthias Hellerer
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 * @date 2016-05-20
 * @brief Collection of simple bit manipulation operations
 */

#ifndef BIT_OPERATIONS_H
#define	BIT_OPERATIONS_H

/*!
 * @brief Set a bit to a certain value
 * 
 * @param b: bit-field to manipulate
 * @param n: bit number to manipulate
 * @param x: value
 */
inline void
set_bit_to(int* const b, const unsigned int n, const unsigned int x);

/*!
 * @brief Set a bit
 * 
 * @param b: bit-field to manipulate
 * @param n: bit number to set
 */
inline void
set_bit(int* const b, const unsigned int n);

/*!
 * @brief Clear a bit
 * 
 * @param b: bit-field to manipulate
 * @param n: bit number to clear
 */
inline void
clear_bit(int* const b, const unsigned int n);

/*!
 * @brief Toggle a bit
 * 
 * @param b: bit-field to manipulate
 * @param n: bit number to toggle
 */
inline void
toggle_bit(int* const b, const unsigned int n);

/*!
 * @brief Check whether a bit is set
 * 
 * @param b: bit-field in which to check
 * @param n: bit number to check
 */
inline int
check_bit(int* const b, const unsigned int n);


#endif	/* BIT_OPERATIONS_H */
