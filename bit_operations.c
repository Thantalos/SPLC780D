
#include "bit_operations.h"

inline void
set_bit_to(int* const b, const unsigned int n, const unsigned int x)
{
    *b ^= (-x ^ *b) & (1 << n);
}

inline void
set_bit(int* const b, const unsigned int n)
{
    *b |= 1 << n;
}

inline void
clear_bit(int* const b, const unsigned int n)
{
    *b &= ~(1 << n);
}

inline void
toggle_bit(int* const b, const unsigned int n)
{
    *b ^= 1 << n;
}

inline int
check_bit(int* const b, const unsigned int n)
{
    return (*b >> n) & 1;
}